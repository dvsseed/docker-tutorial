terraform {
  required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.14"
    }
  }
}

# instance the provider
provider "libvirt" {
  uri = "qemu:///system"
}

resource "libvirt_pool" "ubuntu" {
  name = "ubuntu"
  type = "dir"
  path = "/tmp/terraform-provider-libvirt-pool-ubuntu"
}

resource "libvirt_volume" "ubuntu-qcow2-base" {
  name = "ubuntu-22.04-base.qcow2"
  pool = libvirt_pool.ubuntu.name
  #source = "https://cloud-images.ubuntu.com/releases/jammy/release/ubuntu-22.04-server-cloudimg-amd64.img"
  source = "./ubuntu-22.04-server-cloudimg-amd64.img"
  format = "qcow2"
}

resource "libvirt_volume" "nfsserver_disk" {
  name           = "nfsserver_disk.qcow2"
  pool           = libvirt_pool.ubuntu.name
  base_volume_id = libvirt_volume.ubuntu-qcow2-base.id
  size           = 21474836480
}

resource "libvirt_volume" "client1_disk" {
  name           = "client1_disk.qcow2"
  pool           = libvirt_pool.ubuntu.name
  base_volume_id = libvirt_volume.ubuntu-qcow2-base.id
  size           = 21474836480
}

resource "libvirt_volume" "client2_disk" {
  name           = "client2_disk.qcow2"
  pool           = libvirt_pool.ubuntu.name
  base_volume_id = libvirt_volume.ubuntu-qcow2-base.id
  size           = 21474836480
}

data "template_file" "nfsserver_user_data" {
  template = file("${path.module}/nfsserver_cloud_init.cfg")
}

data "template_file" "client1_user_data" {
  template = file("${path.module}/client1_cloud_init.cfg")
}

data "template_file" "client2_user_data" {
  template = file("${path.module}/client2_cloud_init.cfg")
}

data "template_file" "network_config" {
  template = file("${path.module}/network_config.cfg")
}

# for more info about paramater check this out
# https://github.com/dmacvicar/terraform-provider-libvirt/blob/master/website/docs/r/cloudinit.html.markdown
# Use CloudInit to add our ssh-key to the instance
# you can add also meta_data field
resource "libvirt_cloudinit_disk" "nfsserver_cloudinit" {
  name           = "nfsserver_cloudinit.iso"
  user_data      = data.template_file.nfsserver_user_data.rendered
  network_config = data.template_file.network_config.rendered
  pool           = libvirt_pool.ubuntu.name
}

resource "libvirt_cloudinit_disk" "client1_cloudinit" {
  name           = "client1_cloudinit.iso"
  user_data      = data.template_file.client1_user_data.rendered
  network_config = data.template_file.network_config.rendered
  pool           = libvirt_pool.ubuntu.name
}

resource "libvirt_cloudinit_disk" "client2_cloudinit" {
  name           = "client2_cloudinit.iso"
  user_data      = data.template_file.client2_user_data.rendered
  network_config = data.template_file.network_config.rendered
  pool           = libvirt_pool.ubuntu.name
}

# Create the machine
resource "libvirt_domain" "domain_nfsserver" {
  name   = "ubuntu_nfsserver"
  memory = "512"
  vcpu   = 1

  cloudinit = libvirt_cloudinit_disk.nfsserver_cloudinit.id

  network_interface {
    network_name   = "default"
    wait_for_lease = true
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.nfsserver_disk.id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}

resource "libvirt_domain" "domain-client1" {
  name   = "ubuntu-client1"
  memory = "512"
  vcpu   = 1

  cloudinit = libvirt_cloudinit_disk.client1_cloudinit.id

  network_interface {
    network_name   = "default"
    wait_for_lease = true
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.client1_disk.id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}

resource "libvirt_domain" "domain-client2" {
  name   = "ubuntu-client2"
  memory = "512"
  vcpu   = 1

  cloudinit = libvirt_cloudinit_disk.client2_cloudinit.id

  network_interface {
    network_name   = "default"
    wait_for_lease = true
  }

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.client2_disk.id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }
}

# IPs: use wait_for_lease true or after creation use terraform refresh and terraform show for the ips of domain

output "server_ip" {
  value = libvirt_domain.domain_nfsserver.network_interface[0].addresses[0]
}

output "client1_ip" {
  value = libvirt_domain.domain-client1.network_interface[0].addresses[0]
}

output "client2_ip" {
  value = libvirt_domain.domain-client2.network_interface[0].addresses[0]
}

