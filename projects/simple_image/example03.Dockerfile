FROM ubuntu:22.04

RUN sed -i 's/http:\/\/archive.ubuntu.com\/ubuntu/http:\/\/free.nchc.org.tw\/ubuntu/g' /etc/apt/sources.list

RUN apt-get update && apt-get install -y      \
    build-essential cmake git                 \
 && rm -rf /var/lib/apt/lists/*

WORKDIR /workspace

