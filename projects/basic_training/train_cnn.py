from collections import OrderedDict
import torch
import torchvision

from tqdm import tqdm
from torch import nn
from torch import optim
import torchvision.transforms as T


def get_cnn_model(in_channels, num_classes):
    features = nn.Sequential(
        nn.Conv2d(in_channels, 128, 5, 1, 2),
        nn.ReLU(True),
        nn.MaxPool2d(3, 2),
    )
    avgpool = nn.AdaptiveAvgPool2d((6, 6))
    classifier = nn.Sequential(
        nn.Flatten(),
        nn.Dropout(),
        nn.Linear(128 * 6 * 6, 512),
        nn.ReLU(True),
        nn.Dropout(),
        nn.Linear(512, 512),
        nn.ReLU(True),
        nn.Linear(512, num_classes),
    )

    return nn.Sequential(OrderedDict([("features", features), ("avgpool", avgpool), ("classifier", classifier)]))


def train_one_epoch(device, train_dl, model, loss_fn, optimizer):
    model.train()
    loss_metric = 0.0

    for data, label in tqdm(train_dl):
        data, label = data.to(device), label.to(device)
        output = model(data)

        loss = loss_fn(output, label)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        loss_metric += loss.item()

    loss_metric /= len(train_dl)
    return loss_metric


def test_one_epoch(device, test_dl, model, loss_fn):
    model.eval()

    loss_metric = 0.0
    hit_count = 0
    count = 0

    with torch.no_grad():
        for data, label in tqdm(test_dl):
            data = data.to(device)
            label = label.to(device)

            output = model(data)

            loss = loss_fn(output, label)
            loss_metric += loss.item()

            count += data.shape[0]
            hit_count += torch.eq(output.argmax(dim=1), label).sum().item()

    loss_metric /= len(test_dl)
    accuracy = hit_count / count

    return loss_metric, accuracy


def main():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    train_transform = T.Compose([T.RandomAffine(10, (0.1, 0.1), (0.9, 1.1)), T.ToTensor()])
    test_transform = T.Compose([T.ToTensor()])

    train_ds = torchvision.datasets.FashionMNIST("./dataset", True, train_transform, None, True)
    test_ds = torchvision.datasets.FashionMNIST("./dataset", False, test_transform, None, False)

    train_dl = torch.utils.data.DataLoader(train_ds, 64, True, num_workers=4)
    test_dl = torch.utils.data.DataLoader(test_ds, 64, False, num_workers=4)

    num_classes = len(train_ds.classes)
    max_epochs = 5
    test_interval = 1

    model = get_cnn_model(1, num_classes)
    model = model.to(device)

    loss_fn = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), 0.01)

    best_acc = -1
    best_acc_epoch = -1

    for epoch in range(max_epochs):
        train_loss = train_one_epoch(device, train_dl, model, loss_fn, optimizer)
        print("[{}/{}] train_loss: {:.5f}".format(epoch + 1, max_epochs, train_loss))

        if (epoch + 1) % test_interval == 0:
            test_loss, accuracy = test_one_epoch(device, test_dl, model, loss_fn)

            if accuracy > best_acc:
                best_acc = accuracy
                best_acc_epoch = epoch + 1
                torch.save(model, "best_model.pt")
                print("Save model to best_model.pt")

            print("[{}/{}] test_loss: {:.5f}, accuracy: {:.3f}".format(epoch + 1, max_epochs, test_loss, accuracy))

    print("Training finish")


if __name__ == "__main__":
    main()
