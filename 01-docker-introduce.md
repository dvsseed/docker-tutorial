# Docker介紹+安裝教學

## 介紹

![Docker logo](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/Docker_%28container_engine%29_logo.svg/320px-Docker_%28container_engine%29_logo.svg.png)

Docker是一個開源(Open Source)的容器化平台，使用者可以將程式包裝在容器(Container)中，並且連帶的把程式相依的函式庫(Library)、作業系統包裝在一起。包裝好的應用程式有很好的可攜性，可以輕易的部署到任何地方、搬到別台電腦上執行。

在Docker中執行程式的環境讓人感覺像虛擬機，容器內部與主機環境是隔離的，但容器跟虛擬機在實做的原理上不同。Docker是作業系統層虛擬化，容器與主機共用同一個Linux Kernel；虛擬機則是在硬體上的虛擬化，虛擬機與主機共用主機上的硬體資源。

![Docker-vs-VM](https://www.docker.com/wp-content/uploads/2021/11/docker-containerized-and-vm-transparent-bg-980x401.png.webp)

容器與VM的不專業比較

| Feature            | Container | Virtual Machine |
| ------------------ | --------- | --------------- |
| 啟動速度           | 幾秒鐘    | 幾分鐘          |
| 容量               | MB到GB    | GB起跳          |
| 效能               | 快        | 慢              |
| 支援數量           | 很多個    | 筆電頂多2~3個   |
| 在Linux上跑Windows | 不行      | 可以            |


Docker的執行環境共用Linux Kernel，執行Docker容器不需要開機，僅有應用程式執行，效能非常好；虛擬機的執行，因為要模擬出一台完整電腦，至少要等到虛擬機開機完成，才能開始執行使用者程式，執行的效能受到虛擬化平台(Hypervisor)影響，通常效能有所耗損。


## Docker Engine安裝教學

### Ubuntu的安裝

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

下面是勤勞的人的安裝方法，之前沒有腳本安裝方法，所以都是用下面的指令。

```bash
# 刪掉apt上內建的版本，如果有安裝的話
sudo apt-get remove docker docker-engine docker.io containerd runc

# 安裝相依工具
sudo apt-get install ca-certificates curl gnupg lsb-release

# 新增Docker官方的GPG Key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# 加入Docker官方的apt repo
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update

# 安裝docker
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

參考連結：https://docs.docker.com/engine/install/ubuntu/

### Manjaro或Archlinux的安裝

```bash
# 安裝指令2選1
pacman -S docker
pamac install docker

# 設定開機自動啟動
sudo systemctl enable --now docker
```

### 安裝後設定
```bash
# 檢查docker有無安裝完成並啟動，會看到hello-world和一些介紹。
sudo docker run hello-world

# 把自己的使用者加入docker群組，以後這個使用者使用Docker不需要加sudo。
# 執行後要登出過才會生效。
# 警告：加入docker群組的使用者幾乎相當於有root權限可以操作整個電腦，個人電腦沒關係，但多人共用的電腦要考量權限問題。
sudo adduser ${USER} docker
```

![introduce01.png](assets/introduce01.png)

參考連結：https://docs.docker.com/engine/install/linux-postinstall/

### 解除安裝Docker Engine

```bash
sudo apt-get purge docker-ce docker-ce-cli containerd.io

sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd
```

### 以無Root權限的狀態下執行Docker Engine(Rootless mode)

因為Docker一直被詬病說Docker Engine用root權限執行，讓人覺得權限太大，怕有資安問題。近期才出現Rootless模式，可以以無Root的方式執行Docker Engine，Docker執行的過程中不會需要root權限。

這個方式可能適合多用戶的電腦共用docker(但是作者沒有試過，也不知道nvidia-docker能否支援)。不過，這不是無root的人偷偷安裝Docker的方式，因為一些前置設定仍然需要root權限。

參考連結：https://docs.docker.com/engine/security/rootless/
